package homework1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Lets begin");
        System.out.println("Enter your name");
        Scanner scan = new Scanner(System.in);
        String name = scan.nextLine();
        Random random = new Random();
        int randNum = random.nextInt(100) + 1;
        String[] numbers;
        numbers = new String[100];
        Arrays.fill(numbers,"0");
        int count = 0;
        askNumbers( randNum, numbers,count,name,random);
    }

    public  static void askNumbers(int randNum,String [] numbers,int count,String name,Random random) {
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Please enter your number");
            String nextNum = in.nextLine();

            if (!isInteger(nextNum)) {
                System.out.println("not a number");
            } else if (Arrays.asList(numbers).contains(nextNum)) {
                System.out.println("number already taken");
            } else if (Integer.parseInt(nextNum) < randNum) {
                System.out.println("Your number is too small.Please try again!");
                numbers[count] = nextNum;
                count++;
            } else if (Integer.parseInt(nextNum) > randNum) {
                System.out.println("Your number is too big.Please try again!");
                numbers[count] = nextNum;
                count++;
            } else {
                numbers[count] = nextNum;
                System.out.println("Congratulations" + " " + name);
                System.out.println("Your numbers:");

                int arrLength = 0;
                for (int j = 0; j < numbers.length; j++) {
                    if( Integer.parseInt(numbers[j]) != 0){
                        arrLength++;
                    }
                }
                int[] newArr = new int[arrLength];
                for (int k = 0; k < newArr.length; k++) {
                    if( Integer.parseInt(numbers[k]) != 0){
                        newArr[k] = Integer.parseInt(numbers[k]);
                    }
                }
                bubbleSort(newArr);
                System.out.println(Arrays.toString(newArr));
                break;
            }
        }
        System.out.println("Do you want to continue?-(1- yes or 2 - no)");
        int answer = in.nextInt();
        if (answer == 1) {
            count = 0;
            Arrays.fill(numbers, "0");
            randNum = random.nextInt(100) + 1;
            askNumbers(randNum, numbers, count, name, random);
        } else System.out.println("Bye");
    }
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }
    public static void bubbleSort(int[] intArray) {
        int n = intArray.length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (intArray[j - 1] < intArray[j]) {
                    temp = intArray[j - 1];
                    intArray[j - 1] = intArray[j];
                    intArray[j] = temp;
                }
            }
        }
    }
}